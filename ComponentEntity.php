<?php

namespace Entity\Entities;

use Entity\Entities\Index\AttributeIndex;
use Entity\Entities\Simple\MongoIdAttribute;
use Entity\Exceptions\Entities\EntityDataNotFoundException;
use Entity\Exceptions\Entities\NotRootEntityException;
use Entity\Exceptions\Entities\BadEntityStructureException;
use Entity\Exceptions\Entities\InvalidAttributeNameException;
use Entity\Exceptions\EntityException;
use Entity\Path;
use Entity\Storage\Data\Storage;
use Entity\Storage\Structure\Storage as StructureStorage;
use Entity\ValidatorsManager;
use Entity\Visitor\GuidSearcher;
use Entity\Visitor\Visitor;

/**
 * Class ComponentEntity
 *
 * Содержит общие методы и свойства для классов композитов и листьев.
 * Является частью паттерна Composite (@google: GoF Composite)
 */
abstract class ComponentEntity implements \JsonSerializable
{
    /**
     * @var array
     */
    private $errors = [];
    
    /**
     *
     * @var array
     */
    protected $validators = [];
    /**
     * @var Path
     */
    private $path;

    /**
     * @var array
     */
    private $view = [];

    /**
     * @var string
     */
    protected $name;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * Значение по умолчанию
     *
     * @var
     */
    protected $defaultValue = null;

    /**
     * информация об индексе элемента
     *
     * @var AttributeIndex
     */
    protected $index = null;

    /**
     * Значение должно быть обязательно заполнено
     *
     * @var
     */
    protected $isRequired;

    protected $guid = null;


    /**
     * Создать новыю сущность
     *
     * @param string $name
     * @param Path|null $parentPath
     * @param array|null $view
     * @param array $additionalParams
     * @return static
     */
    public static function createNew($name, $parentPath = null, $view = null, $additionalParams = [])
    {
        $entity = new static($name, $parentPath, $view, $additionalParams);
        return $entity;
    }

    /**
     * Создать компонент по имеющимся данным. Как правило используется для инициализации по данным из базы.
     *
     * @param $data
     * @return \Entity\Entities\ComponentEntity
     */
    public static function restore($data)
    {
        $additionalParams = isset($data['additionalParams']) ? $data['additionalParams'] : [];

        $entity = new static($data['name']);
        /**@var ComponentEntity $entity */

        $entity->setGuid($data['guid']);
        $entity->initialize($data['name'], $data['view'], $additionalParams);
        $entity->getIndex()->resurrection($entity, $data['index']);
        $entity->setDefaultValue($data['defaultValue']);

        return $entity;
    }

    /**
     * Конструктор
     *
     * @param string $name имя элемента
     * @param \Entity\Path|null $parentPath Path, содержащий путь к родительскому элементу. null - если элемент корневой
     * @param array $view все данные, отеносящиеся к представлению
     * @param array|null $additionalParams дополнительные параметры, специфичные для каждой конкретной сущности
     */
    protected function __construct($name, $parentPath = null, $view = null, $additionalParams = [])
    {
        $this->setName($name);
        $this->createPath($parentPath, $name);
        $this->createIndexObject();

        if (!is_null($name)) {
            $this->initialize($name, $view, $additionalParams);
        }
    }

    /**
     * Инициализация основных параметров
     *
     * @param $name
     * @param $view
     * @param $additionalParams
     */
    protected function initialize($name, $view = null, $additionalParams = [])
    {
        $this->setName($name);

        $this->setView($view);

        if (!empty($additionalParams)) {
            $this->setAdditionalParams($additionalParams);
        }
        $this->generateGuid();
    }

	/**
     * Принять Визитора (посетителя)
     *
     * @param Visitor $visitor
     * @return void
     */
    abstract public function accept(Visitor $visitor);


    /**
     * Очистить все данные этого аттрибута перед удалением
     *
     */
    public function removeData()
    {
        Storage::removeAttribute($this->getPath()->getRootName(), $this->getPath()->asString());
        if($this->index !== null) {
            $this->index->deleteIndex();
        }
    }

    /**
     * Создать аттрибут и добавить его сущности
     *
     * @param string $type
     * @param string $name
     * @param array $view
     * @param array|null $additionalParams
     *
     * @return ComponentEntity added Entity
     */
    abstract public function createAndAddAttribute($type, $name, $view = null, $additionalParams = []);

    /**
     * Добавить аттрибут сущности
     *
     * @param ComponentEntity $entity
     * @return mixed
     */
    abstract public function addAttribute(ComponentEntity $entity);

    /**
     * Получить аттрибут сущности по его имени
     *
     * @param string $name
     *
     * @return ComponentEntity
     */
    abstract public function getAttribute($name);

    /**
     * Удалить аттрибут у сущности
     *
     * @param string $name
     *
     * @return $this
     */
    abstract public function removeAttribute($name);

    /**
     * Имеет ли сущность аттрибут с названием name
     *
     * @param string $name
     *
     * @return mixed
     */
    abstract public function hasAttribute($name);

    /**
     * Заменяет атрибут
     *
     * @param \Entity\Entities\ComponentEntity $attribute
     * @return
     * @internal param string $name
     *
     */
    abstract public function changeAttribute(ComponentEntity $attribute);
    
    /**
     * Вернуть все дочерние сущности
     *
     * @return ComponentEntity[]
     */
    abstract public function attributes();
    
    /**
     * 
     * @param string $fromPath
     * @param string $toPath
     */
    abstract public function moveAttribute($fromPath, $toPath, $name);
    
    /**
     * @param string $attributeName
     * @param int $position
     */
    abstract public function setAttributePosition($attributeName, $position);
    
    /**
     * Валидны ли параметры для конкретного типа данных
     *
     * @param $value
     * @return bool
     */
    abstract public function isValid($value);
    
    abstract public function filterValue($value);
    
    /**
     * Validates entity structure
     * 
     * @throws BadEntityStructureException
     */
    public function validateStructure()
    {
        if(empty($this->name)) {
            throw new BadEntityStructureException("Name is not defined");
        }
        
        if(is_null($this->path)) {
            throw new BadEntityStructureException("Path is not defined");
        }
    }

    public function saveStructure()
    {
        if (! $this->getPath()->isRoot()) {
            throw new NotRootEntityException('Cant save structure non root entity:  ' . $this->getPath()->asString());
        }

        StructureStorage::push($this);
        StructureStorage::save($this->getName());
    }

    /**
     * Присвоить дополнительные поля к сущности
     *
     * Дополнительными являются:
     * поля, специфичные для сущности, но необходимые для ее работы на бекенде
     * т.е. их нельзя помещать в массив view
     *
     * Необходимо переопределить метод в дочерних классах, если он нуждается в дополнительной параметризации
     *
     * @param array $params [
     *  'indexed' => bool (optional)
     *  'unique' => bool (optional)
     * ]
     * @return void
     */
    public function setAdditionalParams(array $params)
    {
        if (isset($params['isRequire'])) {
            $this->setIsRequire($params['isRequire']);
        }

        if (isset($params['defaultValue'])) {
            $this->defaultValue = $params['defaultValue'];
        }

        // параметры могут быть не булевыми, а строка "false" or "true"
        if (isset($params['indexed'])) {
            
            $isUnique = false;
            if (array_key_exists('unique', $params)) {
                $isUnique =  $isUnique = filter_var($params['unique'], FILTER_VALIDATE_BOOLEAN);
            }

            $this->changeIndexState(
                $indexed = filter_var($params['indexed'], FILTER_VALIDATE_BOOLEAN),
                $isUnique
            );
        }
    }



    /**
     * @param array|null $view
     */
    public function setView($view = null)
    {
        if (is_null($view) || !is_array($view)) {
            $view = [];
        }
        
        foreach($view as $key => $val) {
            if($val === 'false' || $val === 'true') {
                $view[$key] = filter_var($val, FILTER_VALIDATE_BOOLEAN);
            }
        }
        $this->view = $view;
    }

    /**
     * Смержить отображаемые данные
     *
     * @param array|null $view
     */
    public function mergeView($view = null)
    {
        if (is_null($view)) {
            return;
        }

        $this->setView(array_merge($this->view, $view));
    }

    /**
     * @return array
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param Path $path
     */
    protected function setPath(Path $path)
    {
        $this->path = $path;
    }

    /**
     * @param Path|null $parentPath null if it is the root element
     * TODO вынести в Path.php
     * @param $name
     */
    public function createPath($parentPath, $name)
    {
        $this->setPath(new Path($parentPath, $name));
    }

    /**
     * @return Path
     */
    public function getPath()
    {
        return $this->path;
    }

    //todo this must be observer
    protected function onPathChanged()
    {
        $this->checkAndRepairIndexes();
    }

    /**
     * Событие вызыввается перед удалением элемента
     */
    public function onRemoved()
    {
        if ($this->mustBeIndexed()) {
            $this->deleteIndex();
        }
    }

    /**
     * Присвоить имя сущности
     *
     * Может содержать только латинские буквы, цифры, знак_подчеркивания
     *
     * @param string $name
     *
     * @throws \Entity\Exceptions\Entities\InvalidAttributeNameException
     * @throws \Entity\Exceptions\EntityException
     */
    public function setName($name)
    {
        if (preg_match('/^(\w*)$/', $name) == 0) {
            throw new InvalidAttributeNameException(
                "Invalid name ($name) For attribute. Name must contains only chars: 'a-Z', '0-9' and '_'"
            );
        }

        $this->name = $name;
        if (!is_null($this->path)) {
            $this->getPath()->setName($name);
        }

    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Вызывается когда у родителських элементов сменился путь
     *
     * @param Path $parentPath
     */
    public function onParentPathChanged(Path $parentPath)
    {
        $this->getPath()->setParentPathObject($parentPath);
    }

    public function loadDocument($uid)
    {
        if (! $this->path->isRoot()) {
            throw new NotRootEntityException("Can't load Entity for non-root Object " . $this->getName());
        }

        if (!Storage::has($this->name, $uid)) {
            throw new EntityDataNotFoundException(
                "Cant load entity " . $this->name . ' uid: ' . $uid .  ' because this is not found',
                    [$this->name, $uid]
            );
        }
        $data = Storage::get($this->name, $uid);

        $this->setValue($data);
    }

    public function loadOrCreateDocument($uid)
    {
        if (!Storage::has($this->name, $uid)) {
            $this->getAttribute('_id')->setValue($uid);
        } else {
            $this->loadDocument($uid);
        }
    }

    /**
     * Сохранить данные сущности в постонном хранилище
     *
     * Может быть вызван только у родительского элемента
     *
     * @return mixed
     */
    public function saveDocument()
    {
        if (!$this->getPath()->isRoot()) {
            throw new NotRootEntityException('Can`t save Entity for non-root object');
        }

        $data = $this->getValue();

        Storage::save($this->getName(), $data);
    }

    /**
     * Создать новую сущность
     *
     * @param string $uid опциональный параметр. задается если необходимо создать сущность с конкертным идентфиикатором
     * @throws CantSaveNotRootEntityException
     */
    public function createDocument($uid = null)
    {
        if (! $this->getPath()->isRoot()) {
            throw new NotRootEntityException('Cant create Entity into non-root Object');
        }

        $this->getAttribute('_id')->setValue($uid);
    }

    // todo rename to removeDocument
    /**
     * Удалить документ entity
     *
     * @throws CantSaveNotRootEntityException
     */
    public function removeDocument()
    {
        if (! $this->getPath()->isRoot()) {
            throw new NotRootEntityException('Cant remove Entity into non-root Object');
        }

        Storage::removeDocument($this->getName(), $this->getAttribute('_id')->getValue());
    }

    /**
     * Сохранить значение
     *
     * @param mixed $value
     */
    abstract function setValue($value);

    /**
     * Получить значение
     *
     * @return mixed
     */
    abstract function getValue();
    
    abstract function getNotUniqueFieldValues();

    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * @param mixed $defaultValue
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
    }

    /**
     * Значение должно быть заполнено обязательно?
     *
     * @return bool
     */
    public function isRequired()
    {
        return $this->isRequired;
    }

    /**
     * Установить флаг обязательности заполнения аттрибута
     *
     * @param bool $value
     */
    public function setIsRequire($value)
    {
        $this->isRequired = $value;
    }

    /**
     * Добавить индекс к этому аттрибуту
     *
     * @param bool $isUniq и сделать его уникальным
     * @return null
     */
    public function addIndex($isUniq = false)
    {
        $this->getIndex()->addIndex($isUniq);
    }

    /**
     * Аттрибут должен быть индексированым?
     * @return bool
     */
    public function mustBeIndexed()
    {
        return $this->getIndex()->mustBeIndexed();
    }

    /**
     * Атрибут должен быть уникальным?
     * @return bool
     */
    public function isMustBeUnique()
    {
        return $this->getIndex()->isUniq();
    }

    /**
     * Удалить индекс
     * @return null
     */
    public function deleteIndex()
    {
        $this->getIndex()->deleteIndex();
    }

    /**
     * Добавить или удалить индекс, если это необходимо
     *
     * @param bool $indexed
     * @param bool $isUnique
     */
    public function changeIndexState($indexed, $isUnique)
    {
        $this->getIndex()->changeIndexState($indexed, $isUnique);
    }


    /**
     * Проверить верно ли проставлены индексы в базе
     * Если имеется не соответсвие - правим базу
     *
     * @return null
     */
    public function checkAndRepairIndexes()
    {
        $this->getIndex()->checkAndRepairIndexes();
    }

    /**
     * Создать объект индекса
     */
    protected function createIndexObject()
    {
        $this->index = new AttributeIndex($this);
    }

    /**
     * Get index object
     * 
     * @return \Entity\Entities\Index\AttributeIndex
     */
    protected function getIndex()
    {
        return $this->index;
    }

    /**
     * Здесь содержится методы инициализации свойств. которых может не оказаться
     * в уже созданых и сохраненных объектах entity
     */
    public function __wakeup()
    {
        if (is_null($this->index)) {
            $this->createIndexObject();
        }

        if (is_null($this->getGuid())) {
            $this->generateGuid();
        }
    }


    /**
     * //TODO вынести в rootEntity
     */
    public function addMongoIdAttribute()
    {
        $this->addAttribute(new MongoIdAttribute());
    }

    public function getMongoIdAttribute()
    {
        return $this->getAttribute('_id');
    }

    /**
     * @param $path
     * @return ComponentEntity
     */
    public abstract function getAttributeByPath($path);

    public abstract function hasAttributeByPath($path);
    
    public function getValidators()
    {
        $output = [];
        foreach($this->validators as $name => $params) {
            $output[] = ValidatorsManager::createValidator($name, $params);
        }
        
        return $output;
    }
    
    public function getValidatorsParams()
    {
        $output = [];
        foreach($this->validators as $name => $param) {
            $output = array_merge($output, $param);
        }
        
        return $output;
    }
    
    public function getErrors()
    {
        return $this->errors;
    }
    
    public function hasErrors()
    {
        return count($this->errors) > 0;
    }
    
    protected function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @throws \Entity\Exceptions\EntityException
     * @return string|bool false
     */
    public function getType()
    {
        $className = get_called_class();
        $arr = explode("\\", $className);
        if (!$arr || empty($arr)) {
            throw new EntityException('Cant get entity type');
        }
        return end($arr);
    }

    /**
     * Возвращает имя сущности, включая неймспейсы
     *
     * @return string
     */
    public function getTypeWithNamespaces()
    {
        return '\\' . get_called_class();
    }

    /**
     * Используется для сериализации объекта данного класса в JSON
     *
     * @return array of class properties
     */
    public function toArray()
    {
        $propertiesToSerialize = [
            'name' => $this->getName(),
            'type' => $this->getTypeWithNamespaces(),
            'view' => $this->getView(),
            'isRequired' => $this->isRequired,
            'guid' => $this->getGuid(),
            'defaultValue' => $this->getDefaultValue(),
            'index' => $this->index->jsonSerialize()
        ];

        return $propertiesToSerialize;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }




    public function setGuid($guid)
    {
        $this->guid = $guid;
    }

    /**
     * Сгенерировтаь GUID для текущего элемента.
     *
     * Если Entity уже содержит GUID - произойдет ошибка
     *
     * @throws \Entity\Exceptions\EntityException
     */
    protected function generateGuid()
    {
        if (is_null($this->guid)) {
            $this->guid = uniqid();
        }
    }

    /**
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Найти аттрибут с GUID, начиная от этого узла и до листьев
     *
     * @param $guid
     * @throws \Entity\Exceptions\EntityException
     * @return ComponentEntity|null
     */
    public function getByGuid($guid)
    {
        $guidSearcher = new GuidSearcher();
        $guidSearcher->setGuidForSearch($guid);

        $this->accept($guidSearcher);

        if (!$guidSearcher->isSearchSuccess()) {
            throw new EntityException('Attribute with GUID: "' . $guid . '" not found into entity "' . $this->getPath()->asString(false) . '"');
        }

        return $guidSearcher->getFoundAttribute();
    }
}
