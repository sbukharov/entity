<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sergey
 * Date: 30.08.13
 * Time: 14:32
 * To change this template use File | Settings | File Templates.
 */

namespace Entity\Tests\Unit\Entities;

use Entity\Entities\ComponentEntity;
use Entity\Entities\CompositeEntity;
use Entity\Entities\Simple\IntAttribute;
use Entity\Entities\Simple\YearAttribute;
use Entity\EntityManager;
use Entity\Storage\Structure\Storage;
use Entity\Storage\Structure\TemporaryStorage;

class CompositeEntityDataTest extends \CTestCase
{
    public function testCreateDocumentWithUid()
    {
        $uid = new \MongoId();
        $cars = new CompositeEntity('cars');
        $cars->createDocument($uid);

        $this->assertEquals((string)$uid, $cars->getAttribute('_id')->getValue());
    }



    public function testSaveAndGetEntity()
    {
        $cars = TestValueCreator::createCarCollection();
        Storage::push($cars);
        Storage::save('cars');

        $cars = TestValueCreator::fillCarCollectionData($cars);
        $cars->saveDocument();

        TemporaryStorage::clearStorage();
        $receivedCars = EntityManager::get('cars');
        $receivedCars->loadDocument($cars->getAttribute('_id')->getValue());

        $data = $receivedCars->getValue();

        $expected = [
            'ferrari' => [
                'f50' => [
                    'maxSpeed' => 320,
                    'year' => 1995,
                ],
                'f40' => [
                    'maxSpeed' => 300
                ],
            ],
            'bmw' => []
        ];

        $this->assertEquals($expected,$data);


    }

    public function testGetValue()
    {
        $cars = TestValueCreator::createCarCollection();
        $cars = TestValueCreator::fillCarCollectionData($cars);

        $data = $cars->getValue();

        $expected = [
            'ferrari' => [
                'f50' => [
                    'maxSpeed' => 320,
                    'year' => 1995,
                ],
                'f40' => [
                    'maxSpeed' => 300
                ],
            ],
            'bmw' => []
        ];

        $this->assertEquals($expected,$data);
    }


    public function testLoadDocument()
    {
        $cars = TestValueCreator::createCarCollection();
        Storage::push($cars);
        Storage::save('cars');

        $cars = TestValueCreator::fillCarCollectionData($cars);
        $uid = $cars->$cars->getAttribute('_id')->getValue();
        $cars->saveEntity();

        TemporaryStorage::clearStorage();
        $receivedCars = TestValueCreator::createCarCollection();
        $receivedCars->loadDocument($cars->$cars->getAttribute('_id')->getValue());

        $data = $receivedCars->getValue();

        $expected = [
            'ferrari' => [
                'f50' => [
                    'maxSpeed' => 320,
                    'year' => 1995,
                ],
                'f40' => [
                    'maxSpeed' => 300
                ],
            ],
            'bmw' => []
        ];

        $this->assertEquals($expected,$data);

    }

    public function testRemoveAttribute()
    {
        $cars = TestValueCreator::createCarCollection();
        Storage::push($cars);
        Storage::save('cars');

        $cars = TestValueCreator::fillCarCollectionData($cars);
        $cars->saveDocument();

        TemporaryStorage::clearStorage();
        $receivedCars = EntityManager::get('cars');
        $receivedCars->loadDocument($cars->$cars->getAttribute('_id')->getValue());

        $receivedCars->getAttribute('ferrari')->removeAttribute('f50');
        $data = $receivedCars->getValue();

        $expected = [
            'ferrari' => [
                // f50 was deleted
                'f40' => [
                    'maxSpeed' => 300
                ],
            ],
            'bmw' => []
        ];

        $this->assertEquals($expected,$data);
    }
}