<?php

namespace Entity\Tests\Unit\Entities\Simple;

use Entity\Entities\Simple\IntAttribute;

class IntAttributeTest extends \CTestCase
{
    public function testTrue()
    {
        $this->assertTrue(true);
    }

    public function testIsValidForInteger()
    {
        $int = new IntAttribute('anyName');
        $this->assertTrue($int->isValid(32), "integer value given");
    }

    public function testIsValidForNotInteger()
    {
        $int = new IntAttribute('anyName');
        $this->assertFalse($int->isValid("I am not integer"), "Not integer value Given");
    }

    public function testInValidForCastToInt()
    {
        $int = new IntAttribute('anyName');
        $this->assertTrue($int->isValid("32"), "casting to integer");
    }

    public function testGetValue()
    {
        $maxSpeed = new IntAttribute('maxSpeed');
        $maxSpeed->setValue(290);

        $this->assertEquals(290, $maxSpeed->getValue());
    }
}