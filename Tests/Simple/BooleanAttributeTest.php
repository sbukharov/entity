<?php

namespace Entity\Tests\Unit\Entities\Simple;

use Entity\Entities\Simple\BooleanAttribute;

/**
 * 
 *
 * @author Ildar Apanaev
 */
class BooleanAttributeTest extends \CTestCase {
    public function testBoolean()
    {
        $attr = new BooleanAttribute('test');
        $attr->setValue('false');
        $this->assertTrue($attr->getValue() === false);
        
        $attr->setValue('true');
        
        $this->assertTrue($attr->getValue() === true);
    }

}
