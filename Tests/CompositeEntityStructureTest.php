<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sergey
 * Date: 30.08.13
 * Time: 14:30
 * To change this template use File | Settings | File Templates.
 */

namespace Entity\Tests\Unit\Entities;

use Entity\Entities\ComponentEntity;
use Entity\Entities\CompositeEntity;
use Entity\Entities\Simple\IntAttribute;
use Entity\Entities\Simple\YearAttribute;
use Entity\EntityManager;
use Entity\Storage\Structure\TemporaryStorage;

class CompositeEntityStructureTest extends \CTestCase
{
    public function testConstruct()
    {
        $entity = new CompositeEntity('rootEntity');

        $this->assertEquals('rootEntity', $entity->getName());
        $this->assertInstanceOf('Entity\Path', $entity->getPath(), 'Path must be instance of Path');
        $this->assertEquals('', $entity->getPath()->asString());
    }


    public function testAddAttribute()
    {
        $ferrari = new CompositeEntity('ferrari');
        $maxSpeed = new IntAttribute('maxSpeed');
        $ferrari->addAttribute($maxSpeed);

        $this->assertInstanceOf('Entity\Entities\Simple\IntAttribute', $ferrari->getAttribute('maxSpeed'));
        $this->assertEquals('maxSpeed', $ferrari->getAttribute('maxSpeed')->getPath()->asString());
    }

    public function testCreateAndAddAttribute()
    {
        $entity = new CompositeEntity('root');
        $entity->createAndAddAttribute('CompositeEntity', '1');

        $this->assertInstanceOf('Entity\Entities\CompositeEntity', $entity->getAttribute('1'));
        $this->assertEquals('1', $entity->getAttribute('1')->getName());
        $this->assertEquals('1', $entity->getAttribute('1')->getPath()->asString());
    }

    public function testRemoveAttribute()
    {
        $entity = new CompositeEntity('root');
        $entity->createAndAddAttribute('CompositeEntity', '1');

        $entity->removeAttribute('1');

        $this->assertEquals(1, sizeof($entity->attributes()));
    }

    /**
     * @expectedException Exception
     */
    public function testRemoveAttribute2Times()
    {
        $entity = new CompositeEntity('root');
        $entity->createAndAddAttribute('CompositeEntity', '1');

        $entity->removeAttribute('1');
        $entity->removeAttribute('1');
    }

    public function testCreateAndAddAttributeChain()
    {
        $entity = new CompositeEntity('root');
        $entity1 = $entity->createAndAddAttribute('CompositeEntity', '1');
        $entity2 = $entity1->createAndAddAttribute('CompositeEntity', '2');
        $entity2->createAndAddAttribute('CompositeEntity', '3');


        $entity3 = $entity->getAttribute('1')->getAttribute('2')->getAttribute('3');

        $this->assertEquals('3', $entity3->getName());
    }



    public function testCloneAttributes()
    {
        $entity = new CompositeEntity('root');
        $entity->createAndAddAttribute('CompositeEntity', 'ce');
        $entity->getAttribute('ce')->createAndAddAttribute('text', 'str');

        $cloneEntity = clone $entity;

        $this->assertTrue($cloneEntity->hasAttribute('ce'));
        $this->assertInstanceOf('Entity\Entities\CompositeEntity', $cloneEntity->getAttribute('ce'));
        $this->assertEquals('ce', $cloneEntity->getAttribute('ce')->getName());
        $this->assertEquals('ce', $cloneEntity->getAttribute('ce')->getPath()->asString());
    }


    public function testPath()
    {
        $entity = new CompositeEntity('root');
        $entity1 = $entity->createAndAddAttribute('CompositeEntity', '1');
        $entity2 = $entity1->createAndAddAttribute('CompositeEntity', '2');
        $entity2->createAndAddAttribute('CompositeEntity', '3');


        $entity3 = $entity->getAttribute('1')->getAttribute('2')->getAttribute('3');

        $this->assertEquals('1.2.3', $entity3->getPath()->asString());
    }

    public function testPathWhenParentNameChanged()
    {
        $entity = new CompositeEntity('root');
        $entity1 = $entity->createAndAddAttribute('CompositeEntity', '1');
        $entity2 = $entity1->createAndAddAttribute('CompositeEntity', '2');
        $entity2->createAndAddAttribute('CompositeEntity', '3');

        $entity3 = $entity->getAttribute('1')->getAttribute('2')->getAttribute('3');

        $entity2->setName('newName');
        $this->assertEquals('1.newName.3', $entity3->getPath()->asString(), 'New name must be into path');
    }

    public function testSaveStructure()
    {
        $entity = new CompositeEntity('root', null, ['title' => 'root']);
        $entity1 = $entity->createAndAddAttribute('CompositeEntity', '1');
        $entity2 = $entity1->createAndAddAttribute('CompositeEntity', '2');
        $entity2->createAndAddAttribute('CompositeEntity', '3');
        $entity->saveStructure();
    }

    public function testMergeView()
    {
        $entity = new CompositeEntity('root');
        $entity->setView([
            'title' => 'root element',
            'leaf' => false
        ]);

        $entity->mergeView([
            'title' => 'merged element',
            'color' => 'red'
        ]);

        $this->assertArrayHasKey('title', $entity->getView());
        $this->assertArrayHasKey('leaf', $entity->getView());
        $this->assertArrayHasKey('color', $entity->getView());

        $this->assertEquals('merged element', $entity->getView()['title']);
        $this->assertEquals('red', $entity->getView()['color']);
    }

    public function testMergeViewWithEmptyView()
    {
        $entity = new CompositeEntity('root');

        $entity->mergeView([
            'title' => 'merged element',
            'color' => 'red'
        ]);

        $this->assertArrayHasKey('title', $entity->getView());
        $this->assertArrayHasKey('color', $entity->getView());

        $this->assertEquals('merged element', $entity->getView()['title']);
        $this->assertEquals('red', $entity->getView()['color']);
    }

    public function testGetAttributeByPath()
    {
        $mercedes = new CompositeEntity('mercedes');
        $mercedes->createAndAddAttribute('number', 'maxSpeed');
        $mercedes->createAndAddAttribute('number', 'power');

        $cars = new CompositeEntity('cars');
        $cars->addAttribute($mercedes);

        $maxSpeed = $cars->getAttributeByPath('cars.mercedes.maxSpeed');
        $this->assertInstanceOf('\Entity\Entities\Simple\IntAttribute', $maxSpeed);
        $this->assertEquals('maxSpeed', $maxSpeed->getName());

    }
    
//    public function testComplexTableValidation()
//    {
//        $entity = new CompositeEntity('root');
//        $entity->setView(['type' => 'complexTable']);
//        
//        $column1 = $entity->createAndAddAttribute('compositeentity', 'column1');
//        $column2 = $entity->createAndAddAttribute('compositeentity', 'column2');
//        $column3 = $entity->createAndAddAttribute('compositeentity', 'column3');
//        
//        $column1->createAndAddAttribute('number', 'element1');
//        $column2->createAndAddAttribute('number', 'element1');
//        $column3->createAndAddAttribute('number', 'element1');
//        
//        $entity->validateStructure();
//    }
//    
//    /**
//     * @expectedException \Entity\Exceptions\BadEntityStructureException
//     */
//    public function testComplexTableValidationForBadNumberOfColumns()
//    {
//        $entity = new CompositeEntity('root');
//        $entity->setView(['type' => 'complexTable']);
//        
//        $column1 = $entity->createAndAddAttribute('compositeentity', 'column1');
//        $column2 = $entity->createAndAddAttribute('compositeentity', 'column2');
//        $column3 = $entity->createAndAddAttribute('compositeentity', 'column3');
//        
//        $column1->createAndAddAttribute('number', 'element1');
//        $column2->createAndAddAttribute('number', 'element1');
//        
//        $entity->validateStructure();
//    }

}