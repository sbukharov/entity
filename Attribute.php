<?php

namespace Entity\Entities;

use Entity\Exceptions\LeafEntityNotContainsChildrenException;
use Entity\Exceptions;
use Entity\Entities\ComponentEntity;
use Entity\ValidatorsManager;
use Entity\Visitor\Visitor;

/**
 * Class Attribute
 *
 * Является родителським классом для всех простых объектов - не содержащих в себе других объектов (листьев)
 * Является частью паттерна Composite
 *
 */
abstract class Attribute extends ComponentEntity
{

    /**
     * @param Visitor $visitor
     */
    public function accept(Visitor $visitor)
    {
        $visitor->visitAttribute($this);
    }
    /**
     * Проверка введенных пользователем данных
     * 
     * @param mixed $value
     */
    public function isValid($value)
    {
        $v = $this->getView();
        $errors = [];
        foreach($this->validators as $validatorName => $params) {
            $validator = ValidatorsManager::createValidator($validatorName, $params);
            try {
                $validator->validate($value);
            } catch(\Exception $e) {
                $errors[] = ['fieldId' => $this->getPath()->asString(), 'fieldTitle' => isset($v['title']) ? $v['title'] : '', 'error' => $e->getMessage()];
            }
        }
        $this->setErrors($errors);
        return !$this->hasErrors();
    }
    /**
     * Добавить аттрибут сущности
     *
     * @param ComponentEntity $entity
     * @throws \Entity\Exceptions\LeafEntityNotContainsChildrenException
     * @return mixed
     */
    public function addAttribute(ComponentEntity $entity)
    {
        throw new LeafEntityNotContainsChildrenException('Cant add entity to Attribute ' . $this->getPath()->asString());
    }
    
    /**
     * @inheritdoc
     */
    public function changeAttribute(ComponentEntity $attribute)
    {
        throw new LeafEntityNotContainsChildrenException('Cant add entity to Attribute ' . $this->getPath()->asString());
    }
    
    /**
     * @inheritdoc
     */
    public function moveAttribute($fromPath, $toPath, $name)
    {
        throw new LeafEntityNotContainsChildrenException('Cant move entity ' . $this->getPath()->asString());
    }
    
    /**
     * 
     * @param string $attributeName
     * @param int $position
     * @throws \Entity\Exceptions\LeafEntityNotContainsChildrenException
     */
    public function setAttributePosition($attributeName, $position)
    {
        throw new LeafEntityNotContainsChildrenException('Cant set attribute position. Leaf entity does not contain attributes');
    }


    /**
     * @inheritdoc
     */
    public function createAndAddAttribute($type, $name, $view = null, $additionalParams = [])
    {
        throw new LeafEntityNotContainsChildrenException('Cant add entity to Attribute ' . $this->getPath()->asString());
    }

    /**
     * @inheritdoc
     */
    public function getAttribute($name)
    {
        throw new LeafEntityNotContainsChildrenException('Cant get nested entity because it is a Attribute '
            . $this->getPath()->asString());
    }

    /**
     * @inheritdoc
     */
    public function removeAttribute($name)
    {
        throw new LeafEntityNotContainsChildrenException('Cant remove entity from Attribute ' . $this->getPath()->asString());
    }

    /**
     * Существует ли дочерняя сущность с таким именем
     *
     * @param string $name
     *
     * @return mixed
     */
    public function hasAttribute($name)
    {
        return false;
    }

    /**
     * Вернуть все дочерние сущности
     *
     * @return \Entity\Entities\ComponentEntity[]
     * @throws \Entity\Exceptions\LeafEntityNotContainsChildrenException
     */
    public function attributes()
    {
        throw new LeafEntityNotContainsChildrenException('Leaf Entity can not have a children');
    }

    /**
     * Валидировать значение и присвоить его сущности
     *
     * @param mixed $value
     */
    public function setValue($value)
    {
        $value = $this->filterValue($value);
        
        if($this->isValid($value)) {
            $this->value = $value;
        }
    }

    /**
     * Получить значение сущности
     *
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @inheritdoc
     */
    public function setAdditionalParams(array $params)
    {
        parent::setAdditionalParams($params);
    }


    /**
     * As default not modify value
     * @param mixed $value
     * @return mixed
     */
    public function filterValue($value)
    {
        return $value;
    }
    
    /**
     * Возвращает значения без уникального индекса
     *
     * todo можно вынести в visitor
     *
     * @return array
     */
    public function getNotUniqueFieldValues()
    {
        if($this->isMustBeUnique()) {
            return null;
        }
        return $this->getValue();
    }
    
    /**
     * 
     * @param \Entity\Path|string $path
     * @throws LeafEntityNotContainsChildrenException
     */
    public function getAttributeByPath($path)
    {
        throw new LeafEntityNotContainsChildrenException("Can not perform search in {$this->getName()} entity: Leaf entity does not contains children");
    }

    /**
     *
     * @param $path
     * @throws \Entity\Exceptions\Entity\LeafEntityNotContainsChildrenException
     */
    public function hasAttributeByPath($path)
    {
        throw new LeafEntityNotContainsChildrenException("Can not perform search in {$this->getName()} entity: Leaf entity does not contains children");
    }
}
