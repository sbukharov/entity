<?php

namespace Entity\Entities\Simple;

use Entity\Entities\Attribute;

/**
 * Булев аттрибут
 *
 */
class BooleanAttribute extends Attribute 
{
    protected $validators = [
        'boolean' => []
    ];
    
    public function setValue($value)
    {
        $value = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        parent::setValue($value);
    }

    /**
     *
     * @param mixed $value
     * @return mixed
     */
    public function filterValue($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    }


}
