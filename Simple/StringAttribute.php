<?php

namespace Entity\Entities\Simple;

use Entity\Entities\Attribute;

/**
 * Class StringAttribute
 */
class StringAttribute extends Attribute
{
    protected $validators = [
        'string' => []
    ];
    /**
     * 
     * @param string $value
     * @return string
     */
    public function filterValue($value)
    {
        $value = parent::filterValue($value);
        
        if (empty($value)) {
            return null;
        }

        $value = trim($value);

        // не должны давать вводить пользоватлям html разметку, иначе они порнобанер вставят
        // но кавычки должны вводится и отображаться
        $value = htmlspecialchars($value, ENT_NOQUOTES);
        
        return $value;
    }

}