<?php

namespace Entity\Entities;

use Entity\Entities\Simple\StringAttribute;
use Entity\Entities\Simple\FileAttribute;

/**
 * Class FileEntity
 *
 * Прикрепленные к документу файлы
 *
 * @package Entity\Entities
 */
class FileEntity extends ArrayEntity
{
    public function __construct($name, $parentPath = null, $view = null)
    {
        parent::__construct($name, $parentPath, $view);
        $this->addCommentAttribute();
        $this->addHashAttribute();
        $this->addFileAttribute();
    }

    /**
     * Оверрайдим родительский класс ибо здесь нам не надо инстанцировать атрибуты они у нас по умолчанию создаются
     * в setAdditionalParams
     *
     * @param $data
     */
    public static function restore($data)
    {
        $additionalParams = isset($data['additionalParams']) ? $data['additionalParams'] : [];
        $entity = new static($data['name']);
        /**@var ReferenceEntity $entity */

        $entity->setGuid($data['guid']);
        $entity->initialize($data['name'], $data['view'], $additionalParams);
        $entity->setAdditionalParams($data);

        return $entity;
    }

    protected function addCommentAttribute()
    {
        $commentAttribute = new StringAttribute('comment');
        $commentAttribute->setIsRequire(false);
        $commentAttribute->setView([
            'title' => 'Комментарий к файлу'
        ]);
        $this->addAttribute($commentAttribute);
    }

    protected function addHashAttribute()
    {
        $hashAttribute = new StringAttribute('hash');
        $hashAttribute->setIsRequire(false);
        $hashAttribute->setView([
            'hidden' => true
        ]);
        $this->addAttribute($hashAttribute);
    }

    protected function addFileAttribute()
    {
        $fileAttribute = new FileAttribute('file');
        $fileAttribute->setIsRequire(false);
        $fileAttribute->setView([
            'title' => 'Файл'
        ]);
        $this->addAttribute($fileAttribute);
    }
}