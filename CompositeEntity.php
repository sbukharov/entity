<?php

namespace Entity\Entities;

use Entity\EntityComponentBuilder;
use Entity\Exceptions\Entities\NotRootEntityException;
use Entity\Exceptions\Entities\EntityAlreadyExistsException;
use Entity\Exceptions\Entities\BadEntityStructureException;
use Entity\Exceptions\Entities\InvalidPathArgumentException;
use Entity\Exceptions\Entities\EntityNotFoundException;
use Entity\Entities\ComponentEntity;
use Entity\EntityManager;
use Entity\Path;
use Entity\Storage\Data\Storage;
use Entity\Storage\Structure\Storage as StructureStorage;
use Entity\Visitor\Visitor;
use JsonSchema\Constraints\Object;

/**
 * Class CompositeEntity
 *
 * представляет объект, содержащий в своих свойствах другие объекты
 * Является частью паттерна Composite
 *
 */
class CompositeEntity extends ComponentEntity
{
    /**
     * @var \Entity\Entities\ComponentEntity[]
     */
    protected $attributes = [];

    /**
     * @inheritdoc
     */
    protected function __construct($name, $parentPath = null, $view = null, $additionalParams = [])
    {
        $this->attributes = new \ArrayObject([], \ArrayObject::ARRAY_AS_PROPS);
        parent::__construct($name, $parentPath, $view, $additionalParams);
    }

    /**
     * Принять Визитора (посетителя)
     *
     * @param Visitor $visitor
     */
    public function accept(Visitor $visitor)
    {
        $visitor->visitComposite($this);

        if (!$visitor->isGoInto()) {
            return;
        }

        foreach ($this->attributes as $attribute) {
            $attribute->accept($visitor);
        }

    }

    /**
     * Добавить аттрибут сущности
     *
     * @param ComponentEntity $entity
     * @throws \Entity\Exceptions\EntityAlreadyExistsException
     * @return mixed
     */
    public function addAttribute(ComponentEntity $entity)
    {
        if ($this->hasAttribute($entity->getName())) {
            throw new EntityAlreadyExistsException("Added attribute " . $entity->getName() . " already exist", [$entity->getName()]);
        }

        $entity->createPath($this->getPath(), $entity->getName());

        $this->attributes[$entity->getName()] = $entity;
    }

    /**
     * @inheritdoc
     */
    public function createAndAddAttribute($type, $name, $view = null, $additionalParams = [])
    {
        $entity = EntityComponentBuilder::createComponent($type, $name);
        $entity->createPath($this->getPath(), $name);
        $entity->setView($view);
        $entity->setAdditionalParams($additionalParams);

        $this->addAttribute($entity);

        return $entity;
    }

    /**
     * @inheritdoc
     */
    public function getAttribute($name)
    {
        if (!$this->hasAttribute($name)) {
            throw new EntityNotFoundException("Can't get attribute $name because it's not find into entity "
            . $this->getPath()->asString());
        }

        return $this->attributes[$name];
    }

    /**
     * @inheritdoc
     */
    public function removeAttribute($name, $removeData = true)
    {
        if (!$this->hasAttribute($name)) {
            throw new EntityNotFoundException("Can't remove attribute $name because it's not find into entity "
            . $this->getPath()->asString());
        }

        $this->getAttribute($name)->onRemoved();

        if ($removeData) {
            $this->getAttribute($name)->removeData();
        }

        unset($this->attributes[$name]);
    }

    /**
     * Существует ли дочерняя сущность с таким именем
     *
     * @param string $name
     *
     * @return mixed
     */
    public function hasAttribute($name)
    {
        return array_key_exists($name, $this->attributes);
    }

    /**
     * Изменяет существующий аттрибут на новый (как правило при изменении типа аттрибута)
     * У аттрибутов должно быть одинаковое имя
     * @param \Entity\Entities\ComponentEntity
     * @throws \Exception
     */
    public function changeAttribute(ComponentEntity $newAttribute)
    {
        if (!$this->hasAttribute($newAttribute->getName())) {
            throw new EntityNotFoundException(
                "Can not change attribute: {$newAttribute->getName()}. Reason: Entity {$this->getName()} has no attribute {$newAttribute->getName()}"
            );
        }

        $newAttribute->getPath()->setParentPathObject($this->getPath());

        $this->attributes[$newAttribute->getName()] = $newAttribute;
    }

    /**
     * Перемещает атрибут
     * 
     * @param string $fromPath
     * @param string $toPath
     */
    public function moveAttribute($fromPath, $toPath, $name)
    {
        if (!$this->getPath()->isRoot()) {
            return;
        }

        if (!$fromPath instanceof Path) {
            $fromPath = Path::createFromString($fromPath);
        }

        if (!$toPath instanceof Path) {
            $toPath = Path::createFromString($toPath);
        }

        $this->moveStructureAttribute($fromPath, $toPath, $name);
        $this->moveDataAttribute($fromPath, $toPath, $name);
    }

    protected function moveStructureAttribute(Path $fromPath, Path $toPath, $name)
    {
        $oldParent = $this->getAttributeByPath($fromPath);
        $newParent = $this->getAttributeByPath($toPath);

        $movedAttribute = $oldParent->getAttribute($name);

        $oldParent->removeAttribute($movedAttribute->getName(), $removeData = false);

        $newParent->addAttribute($movedAttribute);
        $newParent->checkAndRepairIndexes();
    }

    /**
     * @param Path $fromPath путь до элемента, откуда нужно переместить
     * @param Path $toPath путь до элемента, куда нужно переместить
     * @param string $name имя перемещаемого элемента
     */
    protected function moveDataAttribute($fromPath, $toPath, $name)
    {
        $fromPath = new Path($fromPath, $name);
        $toPath = new Path($toPath, $name);
        \Entity\Storage\Data\Storage::move($this->getName(), $fromPath->asString(), $toPath->asString());
    }

    /**
     * TODO: getAttributes()
     * Вернуть все дочерние сущности
     *
     * @return \Entity\Entities\ComponentEntity[]
     */
    public function attributes()
    {
        return $this->attributes;
    }

    /**
     * Изменить имя сущности
     *
     * Так же меняется путь у всех дочерних элементов
     *
     * @param string $name
     */
    public function setName($name)
    {
        parent::setName($name);

        $children = $this->attributes();
        foreach ($children as $child) {
            $child->onParentPathChanged($this->getPath());
        }
    }

    /**
     * задать путь аттрибуту
     *
     * Так же уведомляет все вложеные аттрибуты о том что путь сменился
     *
     * @param Path $path
     */
    protected function setPath(Path $path)
    {
        parent::setPath($path);

        foreach ($this->attributes() as $attribute) {
            $attribute->onParentPathChanged($this->getPath());
        }
    }

    /**
     * Оповещает всех своих детей, о том что проиощли какие-то изменения в иерерхии сущности
     *
     * @param Path $parentPath новый путь родительского элемента
     */
    public function onParentPathChanged(Path $parentPath)
    {
        parent::onParentPathChanged($parentPath);

        foreach ($this->attributes() as $childEntity) {
            $childEntity->onParentPathChanged($this->getPath());
        }
    }

    /**
     * Присвоить значение аттрибутам сущности
     *
     * формат значений:
     * [
     *     'first attribute name' => 'value',
     *     'second attribute name' => 'value',
     *     ...
     *  ]
     *
     * @param array $value
     */
    public function setValue($value)
    {
        if (empty($value)) {
            return;
        }

        foreach ($this->attributes() as $attribute) {
            if (!isset($value[$attribute->getName()])) {
                continue;
            }
            $attribute->setValue($value[$attribute->getName()]);
        }
    }

    /**
     * Возвращает значение аттрибутов в формате
     * [
     *   'first attribute name' => 'value',
     *   'second attribute name' => 'value',
     *   ...
     * ]
     *
     * @return array|mixed
     */
    public function getValue()
    {
        $data = [];
        foreach ($this->attributes() as $attribute) {
            $data[(string)$attribute->getName()] = $attribute->getValue();
        }

        // иначе сохранится пустой массив, вместо пустого объекта
        if (empty($data)) {
            $data = new \stdClass();
        }
        return $data;
    }

    /**
     * Создает копии всех вложенных аттрибутов
     */
    public function __clone()
    {
//        $cloneAttributes = new \ArrayObject();
        $cloneAttributes =[];
        foreach ($this->attributes() as $attribute) {
            $cloneAttributes[$attribute->getName()] = clone $attribute;
        }

        $this->attributes = $cloneAttributes;
    }

    public function __wakeup()
    {
        parent::__wakeup();
        if ($this->getPath()->isRoot()) {
            if (!$this->hasAttribute('_id')) {
                $this->addMongoIdAttribute();
            }
        }
    }

    /**
     * Валидны ли параметры для конкретного типа данных
     *
     * @param $value
     * @return bool
     */
    public function isValid($value)
    {
        if (!is_array($value)) {
            return false;
        }
        $errors = [];
        foreach ($value as $key => $attributeValue) {
            if (isset($this->attributes[$key])) {
                $this->attributes[$key]->isValid($attributeValue);
                if ($this->attributes[$key]->hasErrors()) {
                    $errors = array_merge($errors, $this->attributes[$key]->getErrors());
                }
            }
        }
        
        $this->setErrors($errors);

        return count($errors) === 0;
    }

    /**
     * @inheritdoc
     */
    public function checkAndRepairIndexes()
    {
        parent::checkAndRepairIndexes();

        foreach ($this->attributes as $attribute) {
            $attribute->checkAndRepairIndexes();
        }
    }

    /**
     * Validates entity structure
     * 
     * @throws BadEntityStructureException
     */
    public function validateStructure()
    {
        parent::validateStructure();
        
        foreach($this->attributes as $attribute) {
            $attribute->validateStructure();
            
            if($attribute->getPath()->getParentPathObject()->getName() !== $this->getPath()->getName()) {
                throw new BadEntityStructureException("Child attribute {$attribute->getName()} does not belongs to {$this->getName()}");
            }
        }
    }
    
    /**
     * Фильтрация данных
     * 
     * @param mixed $value
     * @return mixed
     */
    public function filterValue($value)
    {
        return $value;
    }


    /**
     *
     * @param \Entity\Path|string $path
     * @throws \Entity\Exceptions\Entities\NotRootEntityException
     * @return \Entity\Entities\ComponentEntity
     * @ return ComponentEntity
     */
    public function getAttributeByPath($path)
    {
        if(!$this->getPath()->isRoot()) {
            throw new NotRootEntityException("{$this->getName()} is not root entity");
        }

        $pathAsArray = $this->preparePath($path);
        $entity = $this;

        foreach ($pathAsArray as $elementName) {
            $entity = $entity->getAttribute($elementName);
        }
        return $entity;
    }

    public static function restore($data)
    {
        $entity = parent::restore($data);

        if (!empty($data['attributes'])) {
            foreach ($data['attributes'] as $attribute) {
                $entityName = $attribute['type'];
                $childEntity = $entityName::restore($attribute);
                $entity->addAttribute($childEntity);
            }
        }
        return $entity;
    }

    /**
     * Проверить существует элемент по заданному пути
     *
     * @param $path
     * @return bool
     */
    public function hasAttributeByPath($path)
    {
        $pathAsArray = $this->preparePath($path);
        $entity = $this;

        foreach ($pathAsArray as $elementName) {
            if ($entity->hasAttribute($elementName)) {
                $entity = $entity->getAttribute($elementName);
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * Проверить корректность типа path
     * Разбить путь на массив
     *
     * @param $path
     * @return array
     * @throws \Entity\Exceptions\Entities\InvalidPathArgumentException
     * @return array
     */
    private function preparePath($path)
    {
        if(!($path instanceof Path) && 'string' != gettype($path)) {
            throw new InvalidPathArgumentException('Unexpected argument type: ' . gettype($path) . ' Expected string or Path', [is_object($path) ? get_class($path): $path]);
        }

        if ('string' == gettype($path)) {
            $path = Path::createFromString($path);
        }

        $pathAsArray = $path->asArray();

        return $pathAsArray;
    }

    /**
     * Используется для сериализации объекта данного класса в JSON
     *
     * @return array of class properties
     */
    public function toArray()
    {
        $propertiesToSerialize = parent::toArray();
        $propertiesToSerialize['attributes'] = [];

        if (!empty($this->attributes)) {
            foreach ($this->attributes as $attr) {
                $propertiesToSerialize['attributes'][$attr->getName()] = $attr->toArray();
            }
        }
        return $propertiesToSerialize;
    }

    public function saveStructure()
    {
        // todo вынести в rootEntity
        // здесь нельзя создавать _id, т.к. когда создается этот элемент, он не привязан ни к одному родительсокму
        //  и формально является корнем, и у него создается _id элмент.
        if ($this->getPath()->isRoot()) {
            if (!$this->hasAttribute('_id')) {
                $this->addMongoIdAttribute();
            }
        }
        parent::saveStructure();
    }


}
